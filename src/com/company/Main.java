package com.company;

public class Main {

    public static void main(String[] args) {
        String word = "RadAr, raDar!";
        String result = word.replaceAll("[ |!|,]", "");
        String reverse = new StringBuilder(result).reverse().toString();

        int answer = result.compareToIgnoreCase(reverse);
        if (answer == 0) {
            System.out.println("Here's a polindrome ladies and gentlemen!");
        } else {
            System.out.println("Try again please");
        }
    }
}